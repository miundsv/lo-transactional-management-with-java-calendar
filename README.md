### Background

This repository should help you to get started with the Google Calendar API using Java and OAuth 2.0.

### Preliminaries

Using Google API's requires us to sign a license agreement, and register our application. Google has updated their API's for authorization to make them OAuth 2.0 compatible (which is not backwards compatible with OAuth 1.0!!). OAuth is a generic authorization standard, and might seem difficult to comprehend. Luckily, we only use it as a consumer, so we skip the theory. In order to register your application and test it's functionality, do the following:

1. Open the [Google API console](https://console.developers.google.com/flows/enableapi?apiid=calendar) and log into your google account.
3. Choose "Create a new project" and press continue. Choose a meaningful project name such as *jee17-app*.
2. Under "APIs and Services" click "Calendar API" (under "G Suite APIs"). Click "Enable".
5. Press *Create Credentials* and choose *Other* as application type. Take note of the Client ID and Client Secret.

### Instructions

The project within the repo contains two classes : *ApplicationTest* and *CalendarAuthorizer*. ApplicationTest contains compilation errors. Do the following:

1. Import the project into Eclipse.
2. Run CalendarAuthorizer and follow the instructions.
3. Resolve the compilation errors in ApplicationTest by entering your client ID, secret, and the refresh token from 2.
4. Run ApplicationTest. The test is expected to print the id and existing calendars for the test user. If it executes without exceptions, then you successfully completed the setup. For me it prints:

Your calendars:
feldob@student.miun.se
Contacts
Helgdagar i Sverige
Week Numbers

